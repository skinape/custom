# Springboot自定义表和字段

#### 介绍
基于springboot写的自定义表和字段的后端代码，支持各种视图分组。本成品仅供参考
核心知识:多级连表查询，多级连表视图
实现流程:
               功能以模块形式，会默认生成模块的查询语句。视图使用AOP切入拼接SQL。分组使用Java8自带的Collectors.groupingBy()方法
目前支持Pgsql


支持:增删改查！


#### 软件架构
Springboot Pgsql


#### 使用说明

1.创建自定义表/字段 192.168.5.27:8080/createTable

```

{
    "table":"a_custom_test",
    "tableName":"自定义测试表",
    "line":[
        {
            "field":"id",
            "field_alias":"",
            "remark":"我是ID",
            "type":"int4",
            "isPrimary":"Y",
            "isGrowth":"Y",
            "isNull":"N",
            "defaultValue":""
        },
        {
            "field":"name",
            "field_alias":"",
            "remark":"用户名",
            "type":"varchar(80)",
            "isPrimary":"N",
            "isGrowth":"N",
            "isNull":"N",
            "defaultValue":"",
            "join_table":"",
            "join_table_field":""
        },
        {
            "field":"num",
            "field_alias":"",
            "remark":"数量",
            "type":"numeric",
            "isPrimary":"N",
            "isGrowth":"N",
            "isNull":"N",
            "defaultValue":"",
            "join_table":"",
            "join_table_field":""
        },
        {
            "field":"time",
            "field_alias":"",
            "remark":"录入时间",
            "type":"timestamp",
            "isPrimary":"N",
            "isGrowth":"N",
            "isNull":"N",
            "defaultValue":"CURRENT_TIMESTAMP",
            "join_table":"",
            "join_table_field":""
        }
    ]
}

```

2.创建模块 192.168.5.29:8080/createModel


```
{
	"id":5,
    "modelName":"生产发料",
    "modelTerminal":"PDA",
    "modelType":"LIST",
    "tableName":"a_custom_order",
    "tableNameAlias":"t0",
    "modelInfo":[
        {
            "modelName":"生产发料 ",
            "tableName":"",
            "tableNameAlias":"",
            "tableField":"supplier_name",
            "tableFieldAlias":"supplierName",
            "joinType":"LEFT JOIN",
            "joinTableList":[
                {
                    "id":1,
                    "table":"a_custom_order",
                    "tableAlias":"t0",
                    "field":"supplier_code",
                    "fieldAlias":"supplierCode",
                    "joinTable":"a_custom_supplier",
                    "joinTableAlias":"t02",
                    "joinField":"supplier_code",
                    "joinFieldAlias":"supplierCode"
                },
                {
                    "id":2,
                    "table":"a_custom_supplier",
                    "tableAlias":"t02",
                    "field":"supplier_name|supplier_code",
                    "fieldAlias":"supplierName|supplierCode",
                    "joinTable":"a_custom_supplier_disbursement",
                    "joinTableAlias":"t021",
                    "joinField":"supplier_name|supplier_code",
                    "joinFieldAlias":"supplierName|supplierCode"
                }
            ],
            "sonModel":"生产发料子表",
            "showName":"生产发料子表数据",
            "controlType":"LIST",
            "showPosition":"10:60",
            "fontSize":14,
            "fontColor":"black",
            "sort":"4-1",
            "isList":"N",
            "isHeadBold":"N",
            "isEditable":"N",
            "isHeadData":"N",
            "controlSize":"40:40",
            "isDatabase":"Y",
            "enumerationKey":"",
            "isNeedData":"N",
            "relationAssembly":"",
            "relationKey":"",
            "defaultValue":"",
            "dataSource":""
        }
    ]
}
```

3.查询 http://192.168.5.29:8080/model


```
参数:
{
    "modelName":"生产发料",
    "showFieldAliasList":"生产订单号|code;产品编号|productCode;产品名称|productName;供应商|supplierName;创建时间|time;供应商出款金额|money;生产订单状态|status",
    "screenList":[
        {
            "field":"productCode",
            "vaule":"000",
            "type":"模糊"
        },
        {
            "field":"money",
            "vaule":1,
            "type":"大于"
        },
        {
            "field":"money",
            "vaule":600,
            "type":"小于"
        }
    ],
    "sortList":[
        {
            "field":"code",
            "type":"Desc"
        }
    ],
    "groupList":[
        {
            "field":"productCode"
        }
    ],
    "paging":{
        "page":0,
        "size":2
    }
}


返回
{
    "total": 4,
    "data": {
        "13000001——>": [
            {
                "生产订单号|code|10:20": "610005",
                "产品编号|productCode|10:40": "13000001",
                "产品名称|productName|10:50": "M1-包装",
                "供应商|supplierName|10:10": "供应商二",
                "创建时间|time|10:30": "2021-12-11T06:46:00.000+0000",
                "供应商出款金额|money|10:10": "100",
                "生产订单状态|status|10:20": "已下达"
            }
        ],
        "1300000002——>": [
            {
                "生产订单号|code|10:20": "610003",
                "产品编号|productCode|10:40": "1300000002",
                "产品名称|productName|10:50": "M3-包装",
                "供应商|supplierName|10:10": "供应商二",
                "创建时间|time|10:30": "2021-09-03T08:18:17.000+0000",
                "供应商出款金额|money|10:10": "100",
                "生产订单状态|status|10:20": "生产中",
                "list1|生产发料子表数据|code|10:60": [
                    {
                        "物料编号|itemCode|10:10": "1300000013",
                        "物料名称|itemName|10:20": "只是为了展示",
                        "数量|num|10:30": 20,
                        "产品编号|productCode|null": null
                    }
                ]
            }
        ]
    },
    "size": 2,
    "fieldAttribute": {
        "生产发料子表数据|code|10:60": "生产发料子表数据|code|LIST|14|black|4-1|N|N|N|N|40:40|Y|null|生产发料子表|Y|null|null|null|null",
        "生产发料主表数据|productCode|10:10": "生产发料主表数据|productCode|LIST|null|null|null|null|null|null|null|null|Y|null|生产发料|Y|null|null|null|null",
        "生产订单状态|status|10:20": "生产订单状态|status|ARRAY|14|black|2-1|Y|N|N|N|10:10|Y|生产订单状态|null|Y|null|null|null|null",
        "创建时间|time|10:30": "创建时间|time|TEXT|14|black|1-2|Y|N|Y|N|10:10|Y|null|null|Y|null|null|null|null",
        "物料名称|itemName|10:20": "物料名称|itemName|TEXT|14|black|2-1|Y|N|N|N|10:10|N|null||Y|null|null|null|null",
        "产品编号|productCode|null": "产品编号|productCode|TEXT|null|null|null|null|null|null|null|null|Y|null|null|Y|null|null|null|null",
        "生产订单号|code|10:20": "生产订单号|code|TEXT|14|black|1-1|Y|N|Y|N|10:10|Y|null|null|Y|null|null|null|null",
        "供应商|supplierName|10:10": "供应商|supplierName|TEXT|14|black|1-1|N|Y|N|Y|10:10|Y|null|null|Y|null|null|null|null",
        "数量|num|10:30": "数量|num|INT|14|black|2-2|Y|N|N|N|10:10|Y|null||Y|null|null|null|null",
        "物料编号|itemCode|10:10": "物料编号|itemCode|TEXT|14|black|1-1|Y|N|N|N|10:10|Y|null|null|Y|null|null|null|null",
        "产品编号|productCode|10:40": "产品编号|productCode|TEXT|141|black|2-1|Y|N|N|N|10:10|Y|null|null|Y|a_custom_order_guanlian1|itemName|null|3",
        "单据类型|type|10:20": "单据类型|type|ARRAY|14|black|2-1|Y|N|N|N|10:10|Y|单据类型||Y|null|null|null|null",
        "产品名称|productName|10:50": "产品名称|productName|TEXT|14|black|3-1|N|N|N|N|10:10|Y|null|生产发料子表|Y|a_custom_order_guanlian1|itemCode|null|3",
        "供应商出款金额|money|10:10": "供应商出款金额|money|TEXT|14|black|1-1|N|Y|N|Y|10:10|Y|null|null|Y|null|null|null|null"
    },
    "enumerationList": {
        "生产订单状态": [
            {
                "enumerationValue": "1",
                "enumerationKey": "生产订单状态"
            },
            {
                "enumerationValue": "2",
                "enumerationKey": "生产订单状态"
            }
        ],
        "单据类型": [
            {
                "enumerationValue": "1",
                "enumerationKey": "单据类型"
            },
            {
                "enumerationValue": "2",
                "enumerationKey": "单据类型"
            }
        ]
    },
    "page": 0
}
```



4.增加/修改 http://192.168.5.29:8080/saveAndFlush

```
{
    "data":[
        {
        	"code":"610005",
            "time":"2021-12-11 14:46",
            "supplierCode":"160002",
            "生产发料子表数据":[
                {
                    "itemCode":"1300000013",
                    "itemName":"只是为了展示",
                    "num":20,
                    "productCode":null
                }
            ]
        },
        {
        	"id":3,
            "status":"1",
            "生产发料子表数据":[
                {
                    "itemCode":"1300000013",
                    "itemName":"只是为了展示",
                    "num":20,
                    "productCode":null
                }
            ]
        }
    ],
    "modelName":"生产发料"
}
```
5.删除 http://192.168.5.29:8080/delete


```
{
    "data":
        {
        	"id":6,
            "status":"1"
         
        }
    ,
    "modelName":"生产发料"
}
```



