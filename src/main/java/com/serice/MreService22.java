//package com.serice;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.aop.QueryViewAndFieldShow;
//import com.enum2.YesNoEnum;
//import com.form.CustomModelForm;
//import com.form.CustomModelInfoForm;
//import com.form.CustomTableForm;
//import com.form.CustomTableLineForm;
//import com.mapper.MryeMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * Created by SkinApe on 2021/8/6.
// */
//@Service
//public class MreService {
//
//
//    @Autowired
//    private MryeMapper mryeMapper;
//
//    /**
//     * 创建表或者列
//     *
//     * @param customTableForm
//     * @return
//     */
//    public void createTable(CustomTableForm customTableForm) {
//        StringBuffer sql = new StringBuffer();
//        List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
//        Map<String, Object> params = new HashMap();
//
//        String table = customTableForm.getTable();
//        List<CustomTableLineForm> customTableLines = customTableForm.getLine();
//
//        //查询表是否存在 如果存在就是新增列
//        sql.append("SELECT COUNT(*) FROM pg_class WHERE relname = '" + table + "';");
//        params.put("sql", sql.toString());
//        Map<String, Object> sqlMap = mryeMapper.search(params);
//        int isCun = ValueUtil.toInt(sqlMap.get("count"));
//
//
//        sql = new StringBuffer();
//        //首先创建自增长
//        for (CustomTableLineForm customTableLine : customTableLines) {
//            if (customTableLine.getIsPrimary().equals(YesNoEnum.YES.getCode())) {
//                sql.append(" CREATE SEQUENCE IF NOT EXISTS " + table + "_" + customTableLine.getField() + "_seq START 1; ");
//            }
//        }
//
//        //不存在表添加表和字段
//        if (isCun == 0) {
//            sql.append(" CREATE TABLE IF NOT EXISTS " + table + " ( ");
//        }
//
//
//        for (int i = 0; i < customTableLines.size(); i++) {
//            if (isCun == 1) {
//                sql.append("ALTER  TABLE  " + table + "  ADD COLUMN  ");
//            }
//            CustomTableLineForm customTableLine = customTableLines.get(i);
//
//            String field = customTableLine.getField();
//            String fieldAlias = customTableLine.getFieldAlias();
//            String type = customTableLine.getType();
//            String isPrimary = customTableLine.getIsPrimary();
//            String isNull = customTableLine.getIsNull();
//            String isGrowth = customTableLine.getIsGrowth();
//            String defaultValue = customTableLine.getDefaultValue();
//            String remark = customTableLine.getRemark();
//            sql.append(field + " " + type);
//            if (isPrimary.equals(YesNoEnum.YES.getCode())) {
//                sql.append(" primary key ");
//            }
//            if (isNull.equals(YesNoEnum.No.getCode())) {
//                sql.append(" NOT NULL ");
//            }
//
//            if (isGrowth.equals(YesNoEnum.YES.getCode())) {
//                sql.append(" DEFAULT nextval('" + table + "_" + field + "_seq'::regclass)");
//            } else {
//                if (!ValueUtil.toStr(defaultValue).equals("")) {
//                    if (type.contains("timestamp")) {
//                        sql.append(" DEFAULT CURRENT_TIMESTAMP ");
//                    } else {
//                        sql.append(" DEFAULT '" + defaultValue + "' ");
//                    }
//                }
//            }
//            if (i < customTableLines.size() - 1 && isCun == 0) {
//                sql.append(",");
//            }
//            if (isCun == 1) {
//                sql.append(";");
//            }
//
//            //新增记录
//
//            Map<String, Object> value = new HashMap<String, Object>();
//            value.put("table_name", table);
//            value.put("field", field);
//            value.put("field_alias", fieldAlias);
//            value.put("type", type);
//            value.put("remark", remark);
//            value.put("is_custom", YesNoEnum.YES.getCode());
//
//            values.add(value);
//        }
//        if (isCun == 0) {
//            sql.append(" );");
//        }
//        //添加备注
//        for (CustomTableLineForm customTableLine : customTableLines) {
//            String name = customTableLine.getField();
//            String remark = customTableLine.getRemark();
//            if (!ValueUtil.toStr(remark).equals("")) {
//                sql.append(" COMMENT ON COLUMN " + table + "." + name + " IS '" + remark + "'; ");
//            }
//
//        }
//
//
//        params.put("sql", sql.toString());
//
//        mryeMapper.update(params);
//
//
////        sql = new StringBuffer("INSERT INTO a_custom_info ( table_name, field, field_alias, type, remark, is_custom ) VALUES");
////        params.put("sql", sql.toString());
////        sqlMap = mryeMapper.search(params);
////        int isCun = ValueUtil.toInt(sqlMap.get("count"));
//
//
//        sql = new StringBuffer("INSERT INTO a_custom_info ( table_name, field, field_alias, type, remark, is_custom ) VALUES");
//        params.put("sql", sql.toString());
//        params.put("sqlValue", "(#{item.table_name},#{item.field},#{item.field_alias},#{item.type},#{item.remark},#{item.is_custom})");
//        mryeMapper.saveBatch(params, values);
//    }
//
//    /**
//     * 拼接查询语句
//     *
//     * @param selectSql
//     * @param joinSql
//     * @param customModelInfoForm
//     * @return
//     */
//    public Map<String, String> splicing(String selectSql, String joinSql, CustomModelInfoForm customModelInfoForm) {
//        Map<String, String> result = new HashMap<>();
//        //是否存在枚举值
//        if (!ValueUtil.toStr(customModelInfoForm.getEnumerationKey()).equals("")) {
//
//            selectSql = selectSql + " eninfo"+customModelInfoForm.getId()+".enumeration_name AS \"" + customModelInfoForm.getShowName() + "|" + customModelInfoForm.getTableFieldAlias() + "\",";
//
//            joinSql = joinSql + " LEFT JOIN a_custom_enumeration_info eninfo"+customModelInfoForm.getId()+" ON '"+customModelInfoForm.getEnumerationKey()+"' = eninfo"+customModelInfoForm.getId()+".enumeration_key AND CAST (" + customModelInfoForm.getTableNameAlias() + "." + customModelInfoForm.getTableField() + " AS VARCHAR) = eninfo"+customModelInfoForm.getId()+".enumeration_value ";
//
//        } else {
//            if (customModelInfoForm.getIsDatabase().equals("Y")) {
//                if (!ValueUtil.toStr(customModelInfoForm.getJoinType()).equals("")) {
//                    //if (customModelInfoForm.getIsMultiTable().equals("Y")){
//                    List<Map> joinList = JSONArray.parseArray(customModelInfoForm.getJoinTable(), Map.class);
//                    //多连表查询的
//                    Collections.sort(joinList, new Comparator<Map>() {
//                        public int compare(Map o1, Map o2) {
//                            Integer name1 = Integer.valueOf(o1.get("id").toString());
//                            Integer name2 = Integer.valueOf(o2.get("id").toString());
//                            return name1.compareTo(name2);
//                        }
//                    });
//
//                    //遍历连接表
//                    for (Map<String, String> join : joinList) {
//                        String table = join.get("table");
//                        String tableAlias = join.get("tableAlias");
//                        String[] fieldList = join.get("field").split("\\|");
//                        String[] fieldAliasList = join.get("fieldAlias").split("\\|");
//
//                        String joinTable = join.get("joinTable");
//                        String joinTableAlias = join.get("joinTableAlias");
//                        String[] joinFieldList = join.get("joinField").split("\\|");
//                        String[] joinFieldAliasList = join.get("joinFieldAlias").split("\\|");
//
//                        joinSql = joinSql + customModelInfoForm.getJoinType() + " " + joinTable + " " + joinTableAlias + " ON ";
//
//                        for (int i = 0; i < fieldList.length; i++) {
//                            String field = fieldList[i];
//                            String joinField = joinFieldList[i];
//                            joinSql = joinSql + joinTableAlias + "." + joinField + "=" + tableAlias + "." + field + " AND ";
//                        }
//
//                        joinSql = joinSql.substring(0, joinSql.length() - 4);
//
//
//                    }
////                }else {
////                    //单连表查询的
////                    joinSql = joinSql + customModelInfoForm.getJoinType() + " " + customModelInfoForm.getJoinTable() + " " + customModelInfoForm.getJoinTableAlias() + " ON " + customModelInfoForm.getTableNameAlias() + "." + customModelInfoForm.getJoinTableFieldSource() + "=" + customModelInfoForm.getJoinTableAlias() + "." + customModelInfoForm.getJoinTableFieldTarget() + " ";
////                }
//                    selectSql = selectSql + customModelInfoForm.getJoinTableAlias();
//                } else {
//                    selectSql = selectSql + customModelInfoForm.getTableNameAlias();
//                }
//                selectSql = selectSql + "." + customModelInfoForm.getTableField();
//            } else {
//
//                selectSql = selectSql + " '' ";
//            }
//            selectSql = selectSql + " AS \"" + customModelInfoForm.getShowName() + "|" + customModelInfoForm.getTableFieldAlias() + "\",";
//        }
//
//        result.put("selectSql", selectSql);
//        result.put("joinSql", joinSql);
//        return result;
//    }
//
//    /**
//     * 拼接子模块
//     *
//     * @param selectSonSql
//     * @param joinSonSql
//     * @param sonQql
//     * @param customModelInfoForm
//     * @return
//     */
//    public List<Map<String, Object>> splicing1(String selectSonSql, String joinSonSql, String sonQql, CustomModelInfoForm customModelInfoForm) {
//        //子模块
//        List<Map<String, Object>> sonSqlMapList = new ArrayList();
//        selectSonSql = selectSonSql.substring(0, selectSonSql.length() - 1);
//
//        sonQql = sonQql + selectSonSql + " FROM " + customModelInfoForm.getJoinTable() + " " + customModelInfoForm.getJoinTableAlias() + joinSonSql;
//
//        sonQql = sonQql + " WHERE " + customModelInfoForm.getJoinTableAlias() + "." + customModelInfoForm.getJoinTableFieldTarget() + "=#{" + customModelInfoForm.getShowName() + "|" + customModelInfoForm.getTableFieldAlias() + "}";
//
//
//        System.out.println("功能子模块Sql: " + sonQql);
//        Map<String, Object> sonSqlMap = new HashMap<>();
//        sonSqlMap.put("code", customModelInfoForm.getShowName() + "|" + customModelInfoForm.getTableFieldAlias());
//        sonSqlMap.put("sonQql", sonQql);
//        sonSqlMapList.add(sonSqlMap);
//
//        return sonSqlMapList;
//    }
//
//
//    /**
//     * 获取模块信息
//     *
//     * @param
//     * @return
//     */
//    public Map<String, Object> viewModel(Map<String, Object> view) {
//
//        Map<String, Object> result = new HashMap<>();
//
//        Map<String, Object> params = new HashMap();
//
//        //一级子模块
//        List<Map<String, Object>> sonSqlMapList = new ArrayList();
//        //二级子模块
//        List<Map<String, Object>> sonSqlMapListTwo = new ArrayList();
//        //枚举名
//        Map<String, String> enumerationMap = new HashMap<>();
//        //枚举数据
//        Map<String, Object> enumerationList = new HashMap<>();
//        String modelName=ValueUtil.toStr(view.get("modelName"));
//        String sql = "SELECT * FROM a_custom_model WHERE model_name=#{code}";
//        params.put("sql", sql);
//        params.put("code", modelName);
//        CustomModelForm customModelForm = BeanConvertUtils.sourceToTarget(BeanConvertUtils.lineToHumpMap(mryeMapper.search(params)), CustomModelForm.class);
//        if (customModelForm != null) {
//            sql = "SELECT * FROM a_custom_model_info WHERE model_name=#{code}";
//            params.put("sql", sql);
//            List<CustomModelInfoForm> customModelInfoFormList = BeanConvertUtils.sourceListToTargetList(BeanConvertUtils.lineToHumpMapList(mryeMapper.searchList(params)), CustomModelInfoForm.class);
//            sql = "SELECT ";
//            String selectSql = " ";
//            String joinSql = " ";
//            for (CustomModelInfoForm customModelInfoForm : customModelInfoFormList) {
//                //没有子模块
//                if (ValueUtil.toStr(customModelInfoForm.getSonModel()).equals("")) {
//                    Map<String, String> data = splicing(selectSql, joinSql, customModelInfoForm);
//                    selectSql = data.get("selectSql");
//                    joinSql = data.get("joinSql");
//                    if (!ValueUtil.toStr(customModelInfoForm.getEnumerationKey()).equals("")) {
//                        enumerationMap.put(customModelInfoForm.getTableFieldAlias(), customModelInfoForm.getEnumerationKey());
//                    }
//                } else {
//                    //有子模块
//                    modelName = customModelInfoForm.getSonModel();
//                    String sonQql = "SELECT * FROM a_custom_model_info WHERE model_name=#{code}";
//                    params.put("code", modelName);
//                    params.put("sql", sonQql);
//                    List<CustomModelInfoForm> customModelInfoFormSonList = BeanConvertUtils.sourceListToTargetList(BeanConvertUtils.lineToHumpMapList(mryeMapper.searchList(params)), CustomModelInfoForm.class);
//                    sonQql = "SELECT ";
//                    String selectSonSql = " ";
//                    String joinSonSql = " ";
//                    for (CustomModelInfoForm customModelInfoSonForm : customModelInfoFormSonList) {
//                        //没有子模块
//                        if (ValueUtil.toStr(customModelInfoSonForm.getSonModel()).equals("")) {
//                            Map<String, String> data = splicing(selectSonSql, joinSonSql, customModelInfoSonForm);
//                            selectSonSql = data.get("selectSql");
//                            joinSonSql = data.get("joinSql");
//                            if (!ValueUtil.toStr(customModelInfoSonForm.getEnumerationKey()).equals("")) {
//                                enumerationMap.put(customModelInfoSonForm.getTableFieldAlias(), customModelInfoSonForm.getEnumerationKey());
//                            }
//                        } else {
//
//                            //还有子模块
//                            modelName = customModelInfoSonForm.getSonModel();
//                            String sonQqlTwo = "SELECT * FROM a_custom_model_info WHERE model_name=#{code}";
//                            params.put("code", modelName);
//                            params.put("sql", sonQqlTwo);
//                            List<CustomModelInfoForm> customModelInfoFormSonListTwo = BeanConvertUtils.sourceListToTargetList(BeanConvertUtils.lineToHumpMapList(mryeMapper.searchList(params)), CustomModelInfoForm.class);
//                            sonQqlTwo = "SELECT ";
//                            String selectSonSqlTwo = " ";
//                            String joinSonSqlTwo = " ";
//                            for (CustomModelInfoForm customModelInfoSonFormTwo : customModelInfoFormSonListTwo) {
//                                //没有子模块
//                                if (ValueUtil.toStr(customModelInfoSonFormTwo.getSonModel()).equals("")) {
//                                    Map<String, String> data = splicing(selectSonSqlTwo, joinSonSqlTwo, customModelInfoSonFormTwo);
//                                    selectSonSqlTwo = data.get("selectSql");
//                                    joinSonSqlTwo = data.get("joinSql");
//                                    if (!ValueUtil.toStr(customModelInfoSonFormTwo.getEnumerationKey()).equals("")) {
//                                        enumerationMap.put(customModelInfoSonFormTwo.getTableFieldAlias(), customModelInfoSonFormTwo.getEnumerationKey());
//                                    }
//                                } else {
//                                    //暂时不能有子模块
//
//                                }
//
//                            }
//                            sonSqlMapListTwo = splicing1(selectSonSqlTwo, joinSonSqlTwo, sonQqlTwo, customModelInfoSonForm);
//
//                        }
//
//                    }
//
//                    sonSqlMapList = splicing1(selectSonSql, joinSonSql, sonQql, customModelInfoForm);
//                }
//
//            }
//            selectSql = selectSql.substring(0, selectSql.length() - 1);
//            sql = sql + selectSql + " FROM " + customModelForm.getTableName() + " " + customModelForm.getTableNameAlias() + joinSql;
//
//
//            for (String key : enumerationMap.keySet()) {
//                //把枚举数据封装进去
//                params.put("sql", "SELECT enumeration_key,enumeration_value,enumeration_value FROM a_custom_enumeration_info WHERE enumeration_key=#{code}");
//                params.put("code", enumerationMap.get(key));
//                List<Map<String, Object>> mapList = BeanConvertUtils.lineToHumpMapList(mryeMapper.searchList(params));
//                enumerationList.put(enumerationMap.get(key), mapList);
//            }
//
//
//            System.out.println("功能SQL: " + sql);
//            params.put("sql", sql);
//            params.put("code", modelName);
//            params.put("view", view);
//            List<Map<String, Object>> mapList = mryeMapper.searchAopList(params);
//
//
//            for (Map<String, Object> map : mapList) {
//                int listSize = 1;
//                for (Map<String, Object> sonSqlMap : sonSqlMapList) {
//                    params.put("sql", sonSqlMap.get("sonQql"));
//                    params.put(ValueUtil.toStr(sonSqlMap.get("code")), map.get(sonSqlMap.get("code")));
//                    List<Map<String, Object>> sonMapList = BeanConvertUtils.lineToHumpMapList(mryeMapper.searchList(params));
//                    if (sonMapList.size() > 0) {
//                        //二级子集
//                        for (Map<String, Object> sonMap : sonMapList) {
//                            if (sonMap.size() > 0) {
//                                int listSizeTwo = 1;
//                                for (Map<String, Object> sonSqlMapTwo : sonSqlMapListTwo) {
//                                    params.put("sql", sonSqlMapTwo.get("sonQql"));
//                                    params.put(ValueUtil.toStr(sonSqlMapTwo.get("code")), sonMap.get(sonSqlMapTwo.get("code")));
//                                    List<Map<String, Object>> sonMapListTwo = BeanConvertUtils.lineToHumpMapList(mryeMapper.searchList(params));
//                                    if (sonMapListTwo.size() > 0) {
//                                        sonMap.put("list" + listSizeTwo, sonMapListTwo);
//                                        listSizeTwo++;
//                                    }
//                                }
//                                map.put("list" + listSize, sonMapList);
//                                listSize++;
//                            }
//                        }
//
//
//                    }
//
//                }
//
//            }
//            List<Map<String,String>> groupList=(List<Map<String,String>>)params.get("groupList");
//            if (groupList!=null&&groupList.size()>0){
//                Map<String, List<Map<String, Object>>> collect = mapList.stream().collect(Collectors.groupingBy(item->{
//                    String condition="";
//                    for (Map<String,String> group:groupList){
//
//                        Map<String, Object> keyMap=parseMapForFilter(item,group.get("field"));
//                        if (keyMap.size()>0){
//                            condition=condition+item.get(keyMap.keySet().iterator().next())+"——>";
//                        }
//                    }
//
//                    return condition;
//                }));
//                result.put("data", collect);
//            }else {
//
//                result.put("data", mapList);
//            }
//
//            result.put("enumerationList", enumerationList);
//
//            return result;
//        }
//        return null;
//    }
//
//    /**
//     * 从map中查询想要的map项，根据key
//     */
//    public static Map<String, Object> parseMapForFilter(Map<String, Object> map,String filters) {
//        if (map == null) {
//            return null;
//        } else {
//            map = map.entrySet().stream()
//                    .filter((e) -> checkKey(e.getKey(), filters))
//                    .collect(Collectors.toMap(
//                            (e) -> (String) e.getKey(),
//                            (e) -> e.getValue()
//                    ));
//        }
//        return map;
//
//    }
//    /**
//     * 通过indexof匹配想要查询的字符
//     */
//    private static boolean checkKey(String key,String filters) {
//        if (key.indexOf(filters) > -1) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//}
