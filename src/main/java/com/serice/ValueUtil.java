package com.serice;

import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;


public class ValueUtil {
    public static int toInt(Object value, Integer defaultValue) {
        try {
            if (value instanceof Number)
                return ((Number)value).intValue();
            else if (value instanceof String)
                return Integer.parseInt((String) value, 10);
            else
                return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static int toInt(Object value) {
        return toInt(value, 0);
    }

    public static long toLong(Object value, Long defaultValue) {
        try {
            if (value instanceof Number)
                return ((Number)value).longValue();
            else if (value instanceof String)
                return Long.parseLong((String) value, 10);
            else
                return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static long toLong(Object value) {
        return toLong(value, null);
    }

    public static short toShort(Object value, Short defaultValue) {
        try {
            if (value instanceof Number)
                return ((Number)value).shortValue();
            else if (value instanceof String)
                return Short.parseShort((String) value, 10);
            else
                return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static short toShort(Object value) {
        return toShort(value, null);
    }

    public static double toDouble(Object value, Double defaultValue) {
        try {
            if (value instanceof Number)
                return ((Number)value).doubleValue();
            else if (value instanceof String)
                return Double.parseDouble((String) value);
            else
                return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static double toDouble(Object value) {
        return toDouble(value, null);
    }

    public static float toFloat(Object value, Float defaultValue) {
        try {
            if (value instanceof Number)
                return ((Number)value).floatValue();
            else if (value instanceof String)
                return Float.parseFloat((String) value);
            else
                return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static float toFloat(Object value) {
        return toFloat(value, null);
    }




    public static byte toByte(Object value, Byte defaultValue) {
        try {
            if (value instanceof Number)
                return ((Number)value).byteValue();
            else if (value instanceof String)
                return Byte.parseByte((String) value);
            else
                return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static byte toByte(Object value) {
        return toByte(value, null);
    }

    public static String toStr(Object value, String defaultValue) {
        if (StringUtils.isEmpty(value))
            return defaultValue;
        if (value instanceof String)
            return (String) value;
        return value.toString();
    }

    public static String toStr(Object value) {
        return toStr(value, "");
    }

    public static BigDecimal toBigDecimal(Object value, BigDecimal defaultValue,int scale){
        if (StringUtils.isEmpty(value))
            return defaultValue;
        try{
            return new BigDecimal(toStr(value).trim()).setScale(scale,BigDecimal.ROUND_DOWN);
        }catch (Exception e){
            return defaultValue;
        }

    }

    public static BigDecimal toBigDecimal(Object value, int scale){
        return toBigDecimal(value,BigDecimal.ZERO).setScale(scale,BigDecimal.ROUND_DOWN);
    }

    public static BigDecimal toBigDecimal(Object value, BigDecimal defaultValue){
        if (StringUtils.isEmpty(value))
            return defaultValue;
        try{
            return new BigDecimal(toStr(value).trim());
        }catch (Exception e){
            return defaultValue;
        }

    }

    public static BigDecimal toBigDecimal(Object value){
        return toBigDecimal(value,BigDecimal.ZERO);
    }


    public static Date toDate(Object value, Date defaultValue){
        if (StringUtils.isEmpty(value))
            return defaultValue;
        try{
            return (Date) value;
        }catch (Exception e){
            return defaultValue;
        }

    }

    public static Date toDate(Object value){
        return toDate(value,null);
    }

    /**
     * 截取字符串
     * @param value
     * @param count
     * @return
     */
    public static String subStr(String value,Integer count){

        if(!StringUtils.isEmpty(value) && value.length()>count){
            int length = value.length();
            return value.substring(length-count,length);
        }
        return toStr(value,"");

    }

    public static String join(String[] array, String slim) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0)
                builder.append(slim);
            builder.append(array[i]);
        }
        return builder.toString();
    }

    public static String join(List<?> array, String slim) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.size(); i++) {
            if (i > 0)
                builder.append(slim);
            builder.append(array.get(i));
        }
        return builder.toString();
    }

    public static boolean checkEmpty(String value) {
        return value == null || "".equals(value);
    }

    public static boolean checkBlank(String value) {
        return value == null || "".equals(value.trim());
    }

    public static String formatDate(Date date, String format) {
        if (date == null)
            return null;
        SimpleDateFormat formater = new SimpleDateFormat(format);
        return formater.format(date);
    }

    public static String digestMD5(String source) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte[] messageDigest = digest.digest(source.getBytes("UTF-8"));
        BigInteger number = new BigInteger(1, messageDigest);
        String hashtext = number.toString(16);
        while (hashtext.length() < 32)
            hashtext = "0" + hashtext;
        return hashtext;
    }

    public static String base64Encode(String source) throws Exception {
        return Base64Utils.encodeToString(source.getBytes("UTF-8"));
    }

    public static String base64Decode(String source) throws Exception {
        return new String(Base64Utils.decodeFromString(source), "UTF-8");
    }

    public static String uuid(){
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-", "");
    }

    public static String urlEncode(String uri) throws Exception {
        return URLEncoder.encode(uri, "UTF-8");
    }

    public static String urlDecode(String source) throws Exception {
        return URLDecoder.decode(source, "UTF-8");
    }

    public static <T> boolean contains (T[] array, T element) {
        if (array != null && array.length > 0)
            return new HashSet<T>(Arrays.<T>asList(array)).contains(element);
        return false;
    }

    public static String[] splitArray(String origin, String split) {
        if (!checkBlank(origin))
            return origin.trim().split("\\s*" + split + "\\s*");
        return null;
    }







}