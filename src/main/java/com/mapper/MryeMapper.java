package com.mapper;


import com.aop.QueryViewAndFieldShow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface MryeMapper {

    /**
     * 通用查询方法(分页)
     *@Param("sql") String sqlStr, @Param("pageSize") int pageSize, @Param("pageNo") int pageNo
     * @param
     * @return
     */
    List<Map<String, Object>> searchSapPage(Map<String, Object> params);

    /**
     * 通用查询方法
     *@Param("sql") String sql  和参数
     * @param params
     * @return
     */
    List<Map<String, Object>> searchList(Map<String, Object> params);

    /**
     * 通用单个查询方法
     *@Param("sql") String sql  和参数
     * @param params
     * @return
     */
    Map<String, Object> search(Map<String, Object> params);

    /**
     * 带入AOP查询方法
     *@Param("sql") String sql  和参数
     * @param params
     * @return
     */
    @QueryViewAndFieldShow
    List<Map<String, Object>> searchAopList(Map<String, Object> params);

    /**
     * 带入AOP单个查询方法
     *@Param("sql") String sql  和参数
     * @param params
     * @return
     */
    Map<String, Object> searchAop(Map<String, Object> params);

    /**
     * 通用保存sap数据库
     *
     * @param params
     */
    int saveSap(Map<String, Object> params);

    /**
     * 通用删除sap数据库
     *
     * @param params
     */
    int delete(Map<String, Object> params);

    /**
     * 通用修改sap数据库
     *
     * @param params
     */
    int update(Map<String, Object> params);

    /**
     * 通用批量添加数据库
     *
     * @param params
     * @param values
     */
    int saveBatch(@Param("params") Map<String, Object> params, @Param("list") List<Map<String, Object>> values);

    /**
     * 通用批量修改  带 in
     *
     * @param params
     * @param inValues
     */
    int updateBatchIn(@Param("params") Map<String, Object> params, @Param("inValues") List<String> inValues);

    /**
     * 通用批量修改
     *
     * @param list
     */
    int updateBatch( @Param("list") List<Map<String, Object>> list);


    /**
     * 执行存储过程
     * @param map
     */
    int updateProc(Map<String, Object> map);

    Object getProc(Map<String, Object> map);

}
