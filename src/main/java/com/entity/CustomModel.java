package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import lombok.*;

/**
 * @Description  
 * @Author  SkinApe
 * @Date 2021-12-22 
 */

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table ( name ="a_custom_model" )
public class CustomModel  implements Serializable {

	private static final long serialVersionUID =  7064080584148707785L;

   	@Column(name = "id" )
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id ;

	/**
	 * 模型名称
	 */
	@Column(name = "model_name" )
	private String modelName ="";

	/**
	 * 终端
	 */
	@Column(name = "model_terminal" )
	private String modelTerminal ="";

	/**
	 * 模块类型
	 */
	@Column(name = "model_type" )
	private String modelType ="";

	/**
	 * 主表名
	 */
	@Column(name = "table_name" )
	private String tableName ="";

	/**
	 * 主表别名
	 */
	@Column(name = "table_name_alias" )
	private String tableNameAlias ="";

}
