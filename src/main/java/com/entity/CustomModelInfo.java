package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;

import lombok.*;

/**
 * @Description  
 * @Author  SkinApe
 * @Date 2021-12-22 
 */

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table ( name ="a_custom_model_info" )
public class CustomModelInfo  implements Serializable {

	private static final long serialVersionUID =  6134823739289041237L;

   	@Column(name = "id" )
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id ;

	/**
	 * 模型名称
	 */
	@Column(name = "model_name" )
	private String modelName ="";

	/**
	 * 主表名
	 */
	@Column(name = "table_name" )
	private String tableName ="";

	/**
	 * 主表别名
	 */
	@Column(name = "table_name_alias" )
	private String tableNameAlias ="";

	/**
	 * 主表/关联表字段名
	 */
	@Column(name = "table_field" )
	private String tableField ="";

	/**
	 * 主表/关联表字段别名
	 */
	@Column(name = "table_field_alias" )
	private String tableFieldAlias ="";

	/**
	 * 关联表
	 */
	@Column(name = "join_table" )
	private String joinTable ="";

	/**
	 * 关联表别名
	 */
	@Column(name = "join_table_alias" )
	private String joinTableAlias ="";

	/**
	 * 关联表目标字段
	 */
	@Column(name = "join_table_field_target" )
	private String joinTableFieldTarget ="";

	/**
	 * 关联表目标字段别名
	 */
	@Column(name = "join_table_field_target_alias" )
	private String joinTableFieldTargetAlias ="";

	/**
	 * 子模型
	 */
	@Column(name = "son_model" )
	private String sonModel ="";

	/**
	 * 显示名称
	 */
	@Column(name = "show_name" )
	private String showName ="";

	/**
	 * 控件类型
	 */
	@Column(name = "control_type" )
	private String controlType ="";

	/**
	 * 显示位置
	 */
	@Column(name = "show_position" )
	private String showPosition ="";

	/**
	 * 字体大小
	 */
	@Column(name = "font_size" )
	private Long fontSize ;

	/**
	 * 字体颜色
	 */
	@Column(name = "font_color" )
	private String fontColor ="";

	/**
	 * 排序
	 */
	@Column(name = "sort" )
	private String sort ="";

	/**
	 * 是否列表显示
	 */
	@Column(name = "is_list" )
	private String isList ="";

	/**
	 * 是否头部加粗
	 */
	@Column(name = "is_head_bold" )
	private String isHeadBold ="";

	/**
	 * 是否可编辑
	 */
	@Column(name = "is_editable" )
	private String isEditable ="";

	/**
	 * 是否头部数据
	 */
	@Column(name = "is_head_data" )
	private String isHeadData ="";

	/**
	 * 控件大小
	 */
	@Column(name = "control_size" )
	private String controlSize ="";

	/**
	 * 关联类型
	 */
	@Column(name = "join_type" )
	private String joinType ="";

	/**
	 * 关联表源字段
	 */
	@Column(name = "join_table_field_source" )
	private String joinTableFieldSource ="";

	/**
	 * 关联表源字段别名
	 */
	@Column(name = "join_table_field_source_alias" )
	private String joinTableFieldSourceAlias ="";

	/**
	 * 是否数据库字段
	 */
	@Column(name = "is_database" )
	private String isDatabase ="";

	/**
	 * 枚举Key
	 */
	@Column(name = "enumeration_key" )
	private String enumerationKey ="";

	/**
	 * 必填数据:Y/N
	 */
	@Column(name = "is_need_data" )
	private String isNeedData ="";

	/**
	 * 关联组件
	 */
	@Column(name = "relation_assembly" )
	private String relationAssembly ="";

	/**
	 * 关联key
	 */
	@Column(name = "relation_key" )
	private String relationKey ="";

	/**
	 * 默认值
	 */
	@Column(name = "default_value" )
	private String defaultValue ="";

	/**
	 * 数据来源 1默认，2枚举，3表里
	 */
	@Column(name = "data_source" )
	private Long dataSource ;

}
