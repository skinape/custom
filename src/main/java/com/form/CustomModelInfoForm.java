package com.form;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

/**
 * @Description  
 * @Author  SkinApe
 * @Date 2021-09-03 
 */


@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class CustomModelInfoForm  implements Serializable {

	private static final long serialVersionUID =  1358812937604277317L;


	@JSONField(ordinal = 1)
	private Long id ;

	/**
	 * 模型名称
	 */
	@JSONField(ordinal = 2)
	private String modelName ="";

	/**
	 * 主表名
	 */
	@JSONField(ordinal = 3)
	private String tableName ="";

	/**
	 * 主表别名
	 */
	@JSONField(ordinal = 4)
	private String tableNameAlias ="";

	/**
	 * 主表/关联表字段名
	 */
	@JSONField(ordinal = 5)
	private String tableField ="";

	/**
	 * 主表/关联表字段别名
	 */
	@JSONField(ordinal = 6)
	private String tableFieldAlias ="";

	/**
	 * 关联类型
	 */
	@JSONField(ordinal = 7)
	private String joinType ="";


	/**
	 * 关联表
	 */
	@JSONField(ordinal = 8)
	private String joinTable ="";

    private List<CustomModelInfoJoinTableForm> joinTableList;
	/**
	 * 关联表别名
	 */

	//private String joinTableAlias ="";



	/**
	 * 子模型
	 */
	@JSONField(ordinal = 9)
	private String sonModel ="";

	/**
	 * 显示名称
	 */
	@JSONField(ordinal = 10)
	private String showName ="";

	/**
	 * 显示值
	 */
	@JSONField(ordinal = 11)
	private String showValue ="";

	/**
	 * 控件类型
	 */
	@JSONField(ordinal = 12)
	private String controlType ="";

	/**
	 * 显示位置
	 */
	@JSONField(ordinal = 13)
	private String showPosition ="";

	/**
	 * 字体大小
	 */
	@JSONField(ordinal = 14)
	private Integer fontSize =0;

	/**
	 * 字体颜色
	 */
	@JSONField(ordinal = 15)
	private String fontColor ="";

	/**
	 * 排序
	 */
	@JSONField(ordinal = 16)
	private String sort ="";

	/**
	 * 是否列表显示
	 */
	@JSONField(ordinal = 17)
	private String isList ="";

	/**
	 * 是否头部加粗
	 */
	@JSONField(ordinal = 18)
	private String isHeadBold ="";

	/**
	 * 是否可编辑
	 */
	@JSONField(ordinal = 19)
	private String isEditable ="";

	/**
	 * 是否头部数据
	 */
	@JSONField(ordinal = 20)
	private String isHeadData ="";

	/**
	 * 控件大小
	 */
	@JSONField(ordinal = 21)
	private String controlSize ="";

	/**
	 * 是否数据库字段
	 */
	@JSONField(ordinal = 22)
	private String isDatabase ="";


	/**
	 * 枚举名
	 */
	@JSONField(ordinal = 23)
	private String enumerationKey="";

	/**
	 * 必填数据 Y/N
	 */
	@JSONField(ordinal = 24)
	private String isNeedData="";

	/**
	 * 关联组件
	 */
	@JSONField(ordinal = 25)
	private String relationAssembly="";

	/**
	 * 关联key
	 */
	@JSONField(ordinal = 26)
	private String relationKey="";

	/**
	 * 默认值
	 */
	@JSONField(ordinal = 27)
	private String defaultValue="";

	/**
	 * 数据来源 1默认，2枚举，3表里
	 */
	@JSONField(ordinal = 28)
	private String dataSource="";
}
