package com.form;

import lombok.Data;

/**
 * Created by SkinApe on 2021/8/6.
 */
@Data
public class CustomTableLineForm {
    private String tableName;
    //字段名
    private String field;
    //字段别名
    private String fieldAlias;
    //备注
    private String remark;
    //常用类型(int4 varchar timestamp numeric )
    private String type;
    //是否主键
    private String isPrimary;
    //是否自增长
    private String isGrowth;
    //是否为NULL
    private String isNull;
    //默认值
    private String defaultValue;
}
