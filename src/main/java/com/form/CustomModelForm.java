package com.form;


import java.io.Serializable;
import java.math.BigDecimal;

import lombok.*;

/**
 * @Description  
 * @Author  SkinApe
 * @Date 2021-09-03 
 */

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomModelForm  implements Serializable {

	private static final long serialVersionUID =  4708615534439677979L;


	private Long id ;

	/**
	 * 模型名称
	 */

	private String modelName ="";

	/**
	 * 终端
	 */

	private String modelTerminal ="";

	/**
	 * 模块类型
	 */

	private String modelType ="";

	/**
	 * 主表名
	 */

	private String tableName ="";

	/**
	 * 主表别名
	 */

	private String tableNameAlias ="";
}
