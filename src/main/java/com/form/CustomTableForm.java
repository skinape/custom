package com.form;

import lombok.Data;

import java.util.List;

/**
 * Created by SkinApe on 2021/8/6.
 */
@Data
public class CustomTableForm {
    private String table;
    private String tableName;
    private List<CustomTableLineForm> line;
}
