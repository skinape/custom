package com.form;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.io.Serializable;

/**
 * Created by SkinApe on 2021/12/22.
 */
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomModelInfoJoinTableForm implements Serializable {

    private static final long serialVersionUID = -15414682948151678L;


    @JSONField(ordinal = 1)
    private Long id ;

    /**
     * 表名
     */
    @JSONField(ordinal = 2)
    private String table ="";

    /**
     * 表别名
     */
    @JSONField(ordinal = 3)
    private String tableAlias ="";

    /**
     * 字段
     */
    @JSONField(ordinal = 4)
    private String field ="";


    /**
     * 字段别名
     */
    @JSONField(ordinal = 5)
    private String fieldAlias ="";


    /**
     * 关联表名
     */
    @JSONField(ordinal = 6)
    private String joinTable ="";

    /**
     * 关联表别名
     */
    @JSONField(ordinal = 7)
    private String joinTableAlias ="";

    /**
     * 关联字段
     */
    @JSONField(ordinal = 8)
    private String joinField ="";

    /**
     * 关联字段别名
     */
    @JSONField(ordinal = 9)
    private String joinFieldAlias ="";

}
