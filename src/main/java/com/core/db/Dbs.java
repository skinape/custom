package com.core.db;

/**
 * Created by lin.wei on 2017-08-29.
 */
public class Dbs {

    private static final ThreadLocal<String> dataSourceKey = ThreadLocal.withInitial(() -> "defaultDataSource");


    public static String getDbSourceKey(){
        return dataSourceKey.get();
    }

    public static void setDbSourceKey(String dbName){
        dataSourceKey.set(dbName);
    }

    public static void clear(){
        dataSourceKey.remove();
    }
}
