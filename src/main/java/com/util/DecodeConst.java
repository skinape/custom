package com.util;

/**
 * 解密字段
 * Created by Administrator on 2017-07-07.
 */
public class DecodeConst {

    public static final String ALGORITHM_AES = "AES";
    public static final String ALGORITHM_DES = "DES";
    public static final String ALGORITHM_3DES = "DESede";
    public static final String TRANSFORMATION_CBC_P5 = "CBC/PKCS5Padding";
    public static final String TRANSFORMATION_ECB_P5 = "ECB/PKCS5Padding";
    public static final byte[] iv = { 0x12, 0x34, 0x56, 0x78, (byte) 0x90,
            (byte) 0xAB, (byte) 0xCD, (byte) 0xEF, 0x12, 0x34, 0x56, 0x78,
            (byte) 0x90, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF };
    public static final String WAP_AXIN_APP_KEY = "abcdef1234567890";

    public static final String DOCODE_CODE = "UTF-8";


}
