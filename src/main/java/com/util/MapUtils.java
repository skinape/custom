package com.util;

import com.google.common.base.CaseFormat;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by SkinApe on 2021/9/3.
 */
public class MapUtils {
    public static Map<String, Object> parse(String mapString) {
        String resultString = mapString.substring(1, mapString.length() - 1);
        String[] objectArrays = resultString.split(", ");
        Map<String, Object> resultMap = new LinkedHashMap();
        String[] var4 = objectArrays;
        int var5 = objectArrays.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            String objectArray = var4[var6];
            String[] resultArrays = objectArray.split("=");
            resultMap.put(resultArrays[0], resultArrays[1]);
        }

        return resultMap;
    }

    public static Map<String, Object> underlineMapConvertCamelMap(Map<String, Object> underlineMap) {
        Map<String, Object> camelMap = new LinkedHashMap();
        Iterator var2 = underlineMap.entrySet().iterator();

        while(var2.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry)var2.next();
            if(StringUtils.contains((String)entry.getKey(), "_")) {
                camelMap.put(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, (String)entry.getKey()), entry.getValue());
            } else {
                camelMap.put(entry.getKey(), entry.getValue());
            }
        }

        return camelMap;
    }
}
