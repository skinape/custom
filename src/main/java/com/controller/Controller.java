package com.controller;


import com.alibaba.fastjson.JSONArray;
import com.form.CustomModelInfoForm;
import com.form.CustomTableForm;
import com.serice.MreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by SkinApe on 2021/8/6.
 */
@RestController
public class Controller {

    @Autowired
    private MreService mreService;

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public String test(){
        return JSONArray.toJSONString(new CustomModelInfoForm());
    }


    /**
     * 创建表或者列
     * @param customTableForm
     * @return
     */
    @RequestMapping(value = "/createTable",method = RequestMethod.POST)
    public String createTable(@RequestBody @Validated CustomTableForm customTableForm){
        mreService.createTable(customTableForm);
        return "OK";
    }


    /**
     * 新增模块
     * @return
     */
    @RequestMapping(value = "/createModel",method = RequestMethod.POST)
    public Object createModel(@RequestBody Map<String, Object> dataBody){
        mreService.createModel(dataBody);
        return "OK";
    }

    /**
     * 查询模块
     * @return
     */
    @RequestMapping(value = "/model",method = RequestMethod.POST)
    public Object model(@RequestBody Map<String, Object> view){
        return mreService.viewModel(view);
    }

    /**
     * 新增修改
     * @return
     */
    @RequestMapping(value = "/saveAndFlush",method = RequestMethod.POST)
    public Object saveAndFlush(@RequestBody Map<String, Object> data){
        return mreService.saveAndFlush(data);
    }
    /**
     * 删除
     * @return
     */
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    public Object delete(@RequestBody Map<String, Object> data){
        return mreService.delete(data);
    }

}
