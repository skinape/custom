//package com.aop;
//
//import com.alibaba.druid.pool.DruidDataSource;
//
//import com.core.db.Dbs;
//import com.core.db.DynamicDataSource;
//import com.enum2.YesNoEnum;
//import com.util.DecodeConst;
//import com.util.DecodeUtils;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
///**
// * 登陆权限验证 切库模块
// */
//@Component
//@Aspect
//public class CutDataSourceAop {
//
//    @Autowired
//    //private DataDictionaryService dataDictionaryService;
//
//    //是否切库  N：不切库  Y:切库
//    @Value("${sys.is-switch-database}")
//    private String isSwitchDatabase;
//
//
//    @Before("@annotation(preAuthorize)")
//    public void checkout( PreAuthorize preAuthorize) throws Throwable {
//
//        //不需要切换
//        DruidDataSource druidDataSource = null;
//        if(YesNoEnum.No.getCode().equals(isSwitchDatabase)){
//            druidDataSource = (DruidDataSource) DynamicDataSource.dataSourcesMap.get(DynamicDataSource.DEFAULT_DATASOURCE);
//            if(druidDataSource == null){
//                throw new BusinessException("数据源不存在");
//            }
//        }else{
//            UserInfo userInfo = (UserInfo) SecurityHelper.getCurrentUserInfo();
//            String mesOdbcKey = ApplicationConst.MES_DATA_SOURCE+userInfo.getCompanyId();
//
////            druidDataSource = (DruidDataSource) DynamicDataSource.dataSourcesMap.get("defaultDataSource");
//            druidDataSource = (DruidDataSource) DynamicDataSource.dataSourcesMap.get(mesOdbcKey);
//            if(druidDataSource != null){
//                Dbs.setDbSourceKey(mesOdbcKey);
//            }else{
//                String mesOdbc = userInfo.getMesDbc();
//                if(mesOdbc != null){
//                    try{
//                        //解码后的值
//                        String decrypt = DecodeUtils.decrypt_base64(DecodeConst.ALGORITHM_AES, mesOdbc, DecodeConst.WAP_AXIN_APP_KEY, DecodeConst.iv);
//
//                        Map<String, Object> mesOdbcMap = JSONUtil.getJsonToMap(decrypt);
//
//                        System.out.println(mesOdbcMap);
//                        DynamicDataSource.setDataSource(mesOdbcKey,mesOdbcMap);
//
//                        //dataDictionaryService.saveRedis(ValueUtil.toStr(userInfo.getCompanyId()));
//
//                    }catch(Exception e){
//
//                    }
//                }
//            }
//        }
//
//
//
//    }
//
//}
