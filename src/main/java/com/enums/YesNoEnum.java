package com.enums;

/**
 * Created by lin.wei on 2019-12-26.
 */
public enum YesNoEnum {
    YES("Y", "是"),

    No("N", "否");


    private String code;

    private String value;

    YesNoEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }


}
