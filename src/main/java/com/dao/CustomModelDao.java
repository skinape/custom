package com.dao;

import com.entity.CustomModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by SkinApe on 2021/12/22.
 */
@Repository
public interface CustomModelDao extends JpaRepository<CustomModel,Long>,JpaSpecificationExecutor<Long> {
    CustomModel findByModelName(String code);
}
