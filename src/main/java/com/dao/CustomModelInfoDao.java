package com.dao;

import com.entity.CustomModelInfo;
import com.form.CustomModelInfoForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SkinApe on 2021/12/22.
 */
@Repository
public interface CustomModelInfoDao extends JpaRepository<CustomModelInfo,Long>,JpaSpecificationExecutor<Long> {

    List<CustomModelInfo> findByModelNameOrderBySort(String code);

}
