# Springboot自定义表和字段

#### Description
基于springboot写的自定义表和字段的后端代码，支持各种视图分组。本成品仅供参考
核心知识:多级连表查询，多级连表视图
实现流程:
               功能以模块形式，会默认生成模块的查询语句。视图使用AOP切入拼接SQL。分组使用Java8自带的Collectors.groupingBy()方法
目前支持Pgsql




#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
